CREATE DATABASE orientationapp CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'oa_user'@'%' IDENTIFIED BY 'oa_user';
GRANT ALL PRIVILEGES ON orientationapp.* TO 'oa_user'@'%' WITH GRANT OPTION;
FLUSH PRIVILEGES;