/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Level;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.MapPoint;

/**
 * @author Bojan Bogojevic
 *
 */
public interface MapPointRepository extends JpaRepository<MapPoint, Long> {

	MapPoint findByLevelAndPointId(Level level, String pointId);
	
	List<MapPoint> findByLevel(Level level);

}
