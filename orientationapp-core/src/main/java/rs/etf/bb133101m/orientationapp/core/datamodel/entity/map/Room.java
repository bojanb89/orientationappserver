/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Bojan Bogojevic
 *
 */
@Entity
@DiscriminatorValue(Marker.DISCRIMINATOR_ROOM)
public class Room extends Marker {

	/**
	 * 
	 */
	private static final long serialVersionUID = -480753715994529920L;

	@NotNull
	@Getter
	@Setter
	@Column(name = "room_name", length = 50, nullable = false)
	private String name;
}
