package rs.etf.bb133101m.orientationapp.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Marker;

public interface MarkerRepository extends JpaRepository<Marker, Long> {

	Marker findByMarkerId(String markerId);
}
