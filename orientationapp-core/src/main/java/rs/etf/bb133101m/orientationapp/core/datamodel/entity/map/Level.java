package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.BaseEntity;

@Entity
@Table(name = "level")
public class Level extends BaseEntity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2029189393522177685L;

	@JsonIgnore
	@Column(name = "client_id", length = 256, nullable = false)
	@Getter
	@Setter
	private String clientId;

	@NotNull
	@Column(name = "name", length = 256, nullable = false)
	@Getter
	@Setter
	private String name;

	@Column(name = "map_width")
	@Getter
	@Setter
	protected Float mapWidth;

	@Column(name = "map_height")
	@Getter
	@Setter
	protected Float mapHeight;


	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "level")
	@Getter
	@Setter
	private List<MapPoint> points;
	
}
