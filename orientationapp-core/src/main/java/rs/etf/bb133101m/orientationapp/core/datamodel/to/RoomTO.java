package rs.etf.bb133101m.orientationapp.core.datamodel.to;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bojan Bogojevic on 24.6.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoomTO extends MarkerTO {
	public String name;

	@JsonCreator
	public RoomTO(@JsonProperty("markerId") String markerId,
			@JsonProperty("markerOrientationAngle") Double markerOrientationAngle,
			@JsonProperty("referenceImage") String referenceImage, @JsonProperty("name") String name) {
		super(markerId, markerOrientationAngle, referenceImage);
		this.name = name;
	}
}
