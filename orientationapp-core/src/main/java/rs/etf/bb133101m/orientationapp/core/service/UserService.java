/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.User;
import rs.etf.bb133101m.orientationapp.core.repository.UserRepository;

/**
 * @author Bojan Bogojevic
 *
 */
@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	/**
	 * This method returns current OAuth 2 user
	 * 
	 * @return
	 */
	public User currentOAuth2User() {

		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		Authentication userAuthentication = oAuth2Authentication.getUserAuthentication();
		UserDetails ud = (UserDetails) userAuthentication.getPrincipal();

		return ud != null ? userRepository.findByUsername(ud.getUsername()) : null;

	}
	/**
	 * @return current OAuth 2 client's id
	 */
	public String currentOAuth2ClientId() {

		OAuth2Authentication oAuth2Authentication = (OAuth2Authentication) SecurityContextHolder.getContext()
				.getAuthentication();

		return oAuth2Authentication.getOAuth2Request().getClientId();
		
	}

}
