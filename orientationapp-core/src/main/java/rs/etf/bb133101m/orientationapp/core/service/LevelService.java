package rs.etf.bb133101m.orientationapp.core.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Level;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.MapPoint;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.MapTO;
import rs.etf.bb133101m.orientationapp.core.repository.LevelRepository;
import rs.etf.bb133101m.orientationapp.core.repository.MapPointRepository;

@Service
public class LevelService {

	@Value("${storageHome}")
	private String storageHome;

	@Autowired
	private LevelRepository levelRepository;

	@Autowired
	private MapPointRepository mapPointRepository;

	@Autowired
	private UserService userService;

	public void addLevel(Level level) {
		String clientId = userService.currentOAuth2ClientId();
		level.setClientId(clientId);
		if (!StringUtils.isEmpty(level.getName())) {
			levelRepository.save(level);
		}
	}

	public List<Level> getLevels() {
		String clientId = userService.currentOAuth2ClientId();
		List<Level> levels = levelRepository.findByClientId(clientId);
		return levels;
	}

	public void removeLevel(Level level) {
		String clientId = userService.currentOAuth2ClientId();
		Level foundLevel = levelRepository.findByClientIdAndName(clientId, level.getName());
		if (foundLevel != null) {

			List<MapPoint> points = mapPointRepository.findByLevel(foundLevel);
			mapPointRepository.delete(points);

			removeLevelDir(clientId, foundLevel.getName());

			levelRepository.delete(foundLevel);
		}
	}

	private void removeLevelDir(String clientId, String name) {
		Path directoryPath = Paths.get(storageHome, clientId, name);
		if (Files.exists(directoryPath)) {
			File directoryFile = directoryPath.toFile();
			try {
				FileUtils.deleteDirectory(directoryFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public void setLevelDimens(String levelName, Float mapWidth, Float mapHeight) {
		String clientId = userService.currentOAuth2ClientId();
		Level level = levelRepository.findByClientIdAndName(clientId, levelName);
		if(level != null) {
			level.setMapWidth(mapWidth);
			level.setMapHeight(mapHeight);
			levelRepository.save(level);
		}
	}

	public void setMapDimensForMap(MapTO mapTO, String levelName) {
		String clientId = userService.currentOAuth2ClientId();
		Level level = levelRepository.findByClientIdAndName(clientId, levelName);
		if(level != null) {
			mapTO.mapWidth = level.getMapWidth();
			mapTO.mapHeight = level.getMapHeight();
		}
	}
}
