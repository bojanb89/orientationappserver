package rs.etf.bb133101m.orientationapp.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Level;

public interface LevelRepository extends JpaRepository<Level, Long>{

	Level findByClientIdAndName(String clientId, String name);

	List<Level> findByClientId(String clientId);
	
	List<Level> findByName(String name);
	
}
