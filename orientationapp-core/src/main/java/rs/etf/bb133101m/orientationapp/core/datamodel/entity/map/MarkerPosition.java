/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Bojan Bogojevic
 *
 */
@Entity
@DiscriminatorValue(MapPoint.DISCRIMINATOR_MARKER_POSITION)
public class MarkerPosition extends MapPoint {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4802597597559473306L;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "markerPosition", cascade = CascadeType.ALL)
	@Getter
	@Setter
	List<Marker> markers;
}
