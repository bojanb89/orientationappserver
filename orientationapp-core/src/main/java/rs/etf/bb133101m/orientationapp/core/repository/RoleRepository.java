/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.Role;

/**
 * @author Bojan Bogojevic
 *
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
	Role findByName(String name);
}
