package rs.etf.bb133101m.orientationapp.core.datamodel.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Created by Bojan Bogojevic on 24.6.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = PointTO.class, name = PointTO.POINT_TYPE),
		@JsonSubTypes.Type(value = MarkerPositionTO.class, name = PointTO.MARKER_POSITION_TYPE) })
public class PointTO {

	public static final String POINT_TYPE = "POINT";
	public static final String MARKER_POSITION_TYPE = "MARKER_POSITION";

	public String pointID;
	public float x;
	public float y;
	public List<String> neighbourIDs;

	@JsonCreator
	public PointTO(@JsonProperty("pointID") String pointID, @JsonProperty("x") float x, @JsonProperty("y") float y) {
		this.pointID = pointID;
		this.x = x;
		this.y = y;
	}
}
