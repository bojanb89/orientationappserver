package rs.etf.bb133101m.orientationapp.core.datamodel.to;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = MarkerTO.class, name = MarkerTO.MARKER_TYPE),
		@JsonSubTypes.Type(value = RoomTO.class, name = MarkerTO.ROOM_TYPE) })
public class MarkerTO {

	public static final String MARKER_TYPE = "MARKER";
	public static final String ROOM_TYPE = "ROOM";

	public String markerId;

	public Double markerOrientationAngle;
	public String referenceImage;

	@JsonCreator
	public MarkerTO(@JsonProperty("markerId") String markerId,
			@JsonProperty("markerOrientationAngle") Double markerOrientationAngle,
			@JsonProperty("referenceImage") String referenceImage) {
		this.markerId = markerId;
		this.markerOrientationAngle = markerOrientationAngle;
		this.referenceImage = referenceImage;
	}
}
