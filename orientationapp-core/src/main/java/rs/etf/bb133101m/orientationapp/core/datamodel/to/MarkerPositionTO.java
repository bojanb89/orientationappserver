package rs.etf.bb133101m.orientationapp.core.datamodel.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Bojan Bogojevic on 24.6.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MarkerPositionTO extends PointTO {
	
	public List<MarkerTO> markers;

	@JsonCreator
	public MarkerPositionTO(@JsonProperty("pointID") String pointID, @JsonProperty("x") float x,
			@JsonProperty("y") float y, @JsonProperty("markers") List<MarkerTO> markers) {
		super(pointID, x, y);
		this.markers = markers;
	}
}
