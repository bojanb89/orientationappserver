/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.dandelion.core.util.StringUtils;

import rs.etf.bb133101m.orientationapp.core.service.StorageService;
import rs.etf.bb133101m.orientationapp.core.service.UserService;

/**
 * @author Bojan Bogojevic
 *
 */
@Service
public class StorageServiceImpl implements StorageService {

	@Value("${storageHome}")
	private String storageHome;

	@Autowired
	private UserService userService;

	@Override
	public void storeImage(String levelName, String filename, InputStream inputStream) throws IOException {

		String clientId = userService.currentOAuth2ClientId();

		if (StringUtils.isNotBlank(clientId) && StringUtils.isNotBlank(levelName)) {
			Path levelPath = Paths.get(storageHome, clientId, levelName);
			Path target = Paths.get(levelPath.toString(), filename);
			if (Files.notExists(levelPath)) {
				Files.createDirectories(levelPath);
			}
			Files.copy(inputStream, target, StandardCopyOption.REPLACE_EXISTING);
		}
	}

}
