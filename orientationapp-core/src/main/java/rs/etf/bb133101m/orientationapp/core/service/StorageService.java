/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.service;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author Bojan Bogojevic
 *
 */
public interface StorageService {
	void storeImage(String level, String filename, InputStream inputStream) throws IOException;
}
