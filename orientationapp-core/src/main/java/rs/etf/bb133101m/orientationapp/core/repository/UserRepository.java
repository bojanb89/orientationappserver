/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.User;

/**
 * @author Bojan Bogojevic
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
}
