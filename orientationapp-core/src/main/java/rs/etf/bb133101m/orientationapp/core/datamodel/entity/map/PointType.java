/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

/**
 * @author Bojan Bogojevic
 *
 */
public enum PointType {
	POINT, MARKER_POSITION
}
