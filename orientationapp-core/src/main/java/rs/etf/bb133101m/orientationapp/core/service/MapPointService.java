/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.service;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.github.dandelion.core.util.StringUtils;

import lombok.Getter;
import lombok.Setter;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Level;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.MapPoint;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Marker;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.MarkerPosition;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.MarkerType;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.PointType;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Room;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.MapTO;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.MarkerPositionTO;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.MarkerTO;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.PointTO;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.RoomTO;
import rs.etf.bb133101m.orientationapp.core.repository.LevelRepository;
import rs.etf.bb133101m.orientationapp.core.repository.MapPointRepository;
import rs.etf.bb133101m.orientationapp.core.repository.MarkerRepository;

/**
 * @author Bojan Bogojevic
 *
 */
@Service
public class MapPointService {

	@Value("${storage.url.pattern}")
	private String storageUrlPattern;

	@Autowired
	private MapPointRepository mapPointRepository;

	@Autowired
	private LevelRepository levelRepository;

	@Autowired
	private UserService userService;

	@Autowired
	private MarkerRepository markerRepository;

	@Getter
	@Setter
	private MapTO mapTO;

	public List<PointTO> toFromPoints(String levelName) {
		String clientId = userService.currentOAuth2ClientId();
		if (StringUtils.isNotBlank(clientId)) {
			Level level;
			List<MapPoint> points;

			// fetch all points for level and client or all for current client
			if (StringUtils.isNotBlank(levelName)) {
				level = levelRepository.findByClientIdAndName(clientId, levelName);
				points = mapPointRepository.findByLevel(level);
			} else {
				List<Level> levels = levelRepository.findByClientId(clientId);
				points = new ArrayList<>();
				levels.forEach(tmpLevel -> {
					points.addAll(mapPointRepository.findByLevel(tmpLevel));
				});
			}

			List<PointTO> pointTOs = new ArrayList<>();
			points.forEach(tmpPoint -> {
				PointTO pointTO;
				if (tmpPoint instanceof MarkerPosition) {
					MarkerPosition tmpMarkerPosition = (MarkerPosition) tmpPoint;
					List<MarkerTO> markerTOs = new ArrayList<>();
					pointTO = new MarkerPositionTO(tmpPoint.getPointId(), tmpPoint.getX(), tmpPoint.getY(), markerTOs);
					if (!CollectionUtils.isEmpty(tmpMarkerPosition.getMarkers())) {
						tmpMarkerPosition.getMarkers().forEach(tmpMarker -> {
							MarkerTO markerTO;

							String referenceImageUrl = formatImageUrl(tmpMarker.getReferenceImage(),
									tmpMarker.getMarkerPosition().getLevel().getName());

							if (tmpMarker instanceof Room) {
								markerTO = new RoomTO(tmpMarker.getMarkerId(), tmpMarker.getMarkerOrientationAngle(),
										referenceImageUrl, ((Room) tmpMarker).getName());
							} else {
								markerTO = new MarkerTO(tmpMarker.getMarkerId(), tmpMarker.getMarkerOrientationAngle(),
										referenceImageUrl);
							}
							markerTOs.add(markerTO);
						});
					}
				} else {
					pointTO = new PointTO(tmpPoint.getPointId(), tmpPoint.getX(), tmpPoint.getY());
				}
				pointTOs.add(pointTO);
			});

			addNeighboursTO(points, pointTOs);

			return pointTOs;
		}
		return null;
	}

	private String formatImageUrl(String imageLocation, String levelName) {
		if (StringUtils.isBlank(imageLocation)) {
			return "";
		} else if (imageLocation.startsWith("http")) {
			return imageLocation;
		} else {
			String clientId = userService.currentOAuth2ClientId();
			if (StringUtils.isBlank(clientId) || StringUtils.isBlank(levelName)) {
				return "";
			} else {
				return MessageFormat.format(storageUrlPattern, levelName + "/" + imageLocation);
			}
		}
	}

	public void savePoints(String levelName, List<PointTO> pointTOs) {

		String clientId = userService.currentOAuth2ClientId();
		if (StringUtils.isBlank(clientId)) {
			// TODO throw error
		}

		Level level = fetchOrCreateLevel(clientId, levelName);
		if (level == null) {
			// TODO throw error
		}

		List<MapPoint> points = new ArrayList<>();
		pointTOs.forEach(tmpPointTO -> {
			MapPoint oldPoint = mapPointRepository.findByLevelAndPointId(level, tmpPointTO.pointID);
			final MapPoint tmpPoint;
			if (tmpPointTO instanceof MarkerPositionTO) {
				if (oldPoint == null || !(oldPoint instanceof MarkerPosition)) {
					tmpPoint = new MarkerPosition();
					if (oldPoint != null) {
						tmpPoint.setId(oldPoint.getId());
					}
				} else {
					tmpPoint = oldPoint;
				}
				tmpPoint.setPointType(PointType.MARKER_POSITION);

				List<Marker> markers = new ArrayList<>();
				List<MarkerTO> markerTOs = ((MarkerPositionTO) tmpPointTO).markers;
				if (!CollectionUtils.isEmpty(markerTOs)) {
					markerTOs.forEach(tmpMarkerTO -> {
						Marker oldMarker = markerRepository.findByMarkerId(tmpMarkerTO.markerId);

						Marker tmpMarker = oldMarker;
						if (tmpMarkerTO instanceof RoomTO) {
							if (oldMarker == null || !(oldMarker instanceof Room)) {
								tmpMarker = new Room();
								if (oldMarker != null) {
									tmpMarker.setId(oldMarker.getId());
								}
							} else {
								tmpMarker = oldMarker;
							}
							tmpMarker.setMarkerType(MarkerType.ROOM);
							((Room) tmpMarker).setName(((RoomTO) tmpMarkerTO).name);
						}

						if (tmpMarker == null) {
							if (oldMarker == null || oldMarker instanceof Room) {
								tmpMarker = new Marker();
								if (oldMarker != null) {
									tmpMarker.setId(oldMarker.getId());
								}
							}
							tmpMarker.setMarkerType(MarkerType.MARKER);
						}
						tmpMarker.setMarkerId(tmpMarkerTO.markerId);
						tmpMarker.setMarkerOrientationAngle(tmpMarkerTO.markerOrientationAngle);
						tmpMarker.setReferenceImage(tmpMarkerTO.referenceImage);
						tmpMarker.setMarkerPosition((MarkerPosition) tmpPoint);
						markers.add(tmpMarker);
					});
				}
				((MarkerPosition) tmpPoint).setMarkers(markers);

			} else {
				if (oldPoint == null || oldPoint instanceof MarkerPosition) {
					tmpPoint = new MapPoint();
					if (oldPoint != null) {
						tmpPoint.setId(oldPoint.getId());
					}
				} else {
					tmpPoint = oldPoint;
				}
				tmpPoint.setPointType(PointType.POINT);
			}

			tmpPoint.setX(tmpPointTO.x);
			tmpPoint.setY(tmpPointTO.y);
			tmpPoint.setPointId(tmpPointTO.pointID);
			tmpPoint.setLevel(level);
			mapPointRepository.save(tmpPoint);

			points.add(tmpPoint);

		});

		mapPointRepository.flush();
		addNeighbours(points, pointTOs);
		mapPointRepository.save(points);

	}

	private Level fetchOrCreateLevel(String clientId, String levelName) {

		Level level = levelRepository.findByClientIdAndName(clientId, levelName);
		if (level == null) {
			level = new Level();
			level.setClientId(clientId);
			level.setName(levelName);
			level = levelRepository.save(level);
		}
		return level;
	}

	private void addNeighbours(List<MapPoint> points, List<PointTO> pointTOs) {

		HashMap<String, MapPoint> hmap = new HashMap<String, MapPoint>();
		for (MapPoint mapPoint : points) {
			hmap.put(mapPoint.getPointId(), mapPoint);
		}

		for (MapPoint tmpPoint : points) {
			Set<MapPoint> oldNeighbours = tmpPoint.getNeighbours();
			Set<MapPoint> newNeighbours = new HashSet<>();
			for (PointTO tmpPointTO : pointTOs) {

				if (tmpPointTO.pointID != null && tmpPointTO.pointID.equals(tmpPoint.getPointId())
						&& tmpPointTO.neighbourIDs != null) {
					if (oldNeighbours != null) {
						List<MapPoint> neighboursToRemove = new ArrayList<>();
						for (MapPoint oldNeighbour : oldNeighbours) {
							if (tmpPointTO.neighbourIDs.contains(oldNeighbour.getPointId())) {
								tmpPointTO.neighbourIDs.remove(oldNeighbour.getPointId());
							} else {
								// id was deleted
								neighboursToRemove.add(oldNeighbour);
							}
						}
						oldNeighbours.removeAll(neighboursToRemove);

					}

					for (String neighbourId : tmpPointTO.neighbourIDs) {
						newNeighbours.add(hmap.get(neighbourId));
					}
				}
			}
			if (oldNeighbours != null) {
				newNeighbours.addAll(oldNeighbours);
			}
			tmpPoint.setNeighbours(newNeighbours);
		}
	}

	private void addNeighboursTO(List<MapPoint> points, List<PointTO> pointTOs) {

		for (int i = 0; i < points.size() && i < pointTOs.size(); i++) {
			List<String> neighboursTO = new ArrayList<>();
			MapPoint tmpPoint = points.get(i);
			if (tmpPoint.getNeighbours() != null) {
				for (MapPoint neighbour : tmpPoint.getNeighbours()) {
					neighboursTO.add(neighbour.getPointId());
				}
			}
			PointTO tmpPointTO = pointTOs.get(i);
			tmpPointTO.neighbourIDs = neighboursTO;
		}
	}

	public List<PointTO> getPoints(String levelName) {
		return toFromPoints(levelName);
	}

}
