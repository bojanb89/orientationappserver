/**
 * 
 */
package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.BaseEntity;

/**
 * @author Bojan Bogojevic
 *
 */
@Entity
@Table(name = "map_point")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "point_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(MapPoint.DISCRIMINATOR_POINT)
public class MapPoint extends BaseEntity implements Serializable {

	public static final String DISCRIMINATOR_POINT = "POINT";
	public static final String DISCRIMINATOR_MARKER_POSITION = "MARKER_POSITION";

	/**
	 * 
	 */
	private static final long serialVersionUID = -7129228620910810312L;

	@Enumerated(EnumType.STRING)
	@Column(name = "point_type", insertable = false, updatable = false)
	@Getter
	@Setter
	protected PointType pointType;

	@ManyToOne
	@JoinColumn(name = "level_id")
	@Getter
	@Setter
	private Level level;

	@Column(name = "point_id", length = 256, nullable = false)
	@Getter
	@Setter
	protected String pointId;

	@Column(name = "x", nullable = false)
	@Getter
	@Setter
	protected Float x;

	@Column(name = "y", nullable = false)
	@Getter
	@Setter
	protected Float y;

	@ManyToMany
	@JoinTable(name = "neighbours", joinColumns = { @JoinColumn(name = "point_id") }, inverseJoinColumns = {
			@JoinColumn(name = "neighbour_id") })
	@Getter
	@Setter
	protected Set<MapPoint> neighbours;
}
