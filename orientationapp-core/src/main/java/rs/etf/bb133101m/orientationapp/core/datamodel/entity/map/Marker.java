package rs.etf.bb133101m.orientationapp.core.datamodel.entity.map;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.BaseEntity;

@Entity
@Table(name = "marker")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "marker_type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(Marker.DISCRIMINATOR_MARKER)
public class Marker extends BaseEntity implements Serializable {

	public static final String DISCRIMINATOR_MARKER = "MARKER";
	public static final String DISCRIMINATOR_ROOM = "ROOM";

	/**
	 * 
	 */
	private static final long serialVersionUID = -2010231431716090496L;

	@Enumerated(EnumType.STRING)
	@Column(name = "marker_type", insertable = false, updatable = false)
	@Getter
	@Setter
	protected MarkerType markerType;

	@ManyToOne
	@JoinColumn(name = "point_id")
	@Getter
	@Setter
	private MarkerPosition markerPosition;

	@Column(name = "marker_id", length = 256, nullable = false)
	@Getter
	@Setter
	protected String markerId;

	@Getter
	@Setter
	@Column(name = "marker_orientation_angle")
	protected Double markerOrientationAngle;

	@Getter
	@Setter
	@Column(name = "reference_image", length = 256)
	protected String referenceImage;
}
