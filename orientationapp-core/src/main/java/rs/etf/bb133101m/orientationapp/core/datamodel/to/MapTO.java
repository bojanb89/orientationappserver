package rs.etf.bb133101m.orientationapp.core.datamodel.to;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Bojan Bogojevic on 24.6.2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class MapTO {

	public List<PointTO> pointTOs;
	
	public Float mapWidth;
	
	public Float mapHeight;
}
