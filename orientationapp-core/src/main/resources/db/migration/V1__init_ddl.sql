
USE `orientationapp`;

--------------------------------------------------
-- SPRING OAUTH 2 DATA TABLES:
--------------------------------------------------
--
-- Table structure for table `oauth_client_details`
--
create table `oauth_client_details` (
    client_id VARCHAR(256) PRIMARY KEY,
    resource_ids VARCHAR(256),
    client_secret VARCHAR(256),
    scope VARCHAR(256),
    authorized_grant_types VARCHAR(256),
    web_server_redirect_uri VARCHAR(256),
    authorities VARCHAR(256),
    access_token_validity INTEGER(32),
    refresh_token_validity INTEGER(32),
    additional_information VARCHAR(4096),
    autoapprove VARCHAR(256)
);
--ENGINE = InnoDB
--DEFAULT CHARACTER SET = utf8;



--
-- Table structure for table `oauth_client_token`
--
create table `oauth_client_token` (
    token_id VARCHAR(256),
    token BLOB,
    authentication_id VARCHAR(256),
    user_name VARCHAR(256),
    client_id VARCHAR(256)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


--
-- Table structure for table `oauth_refresh_token`
--
create table `oauth_refresh_token` (
    token_id VARCHAR(256),
    token BLOB,
    authentication BLOB
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



--
-- Table structure for table `oauth_access_token`
--
create table `oauth_access_token` (
    token_id VARCHAR(256),
    token BLOB,
    authentication_id VARCHAR(256),
    user_name VARCHAR(256),
    client_id VARCHAR(256),
    authentication BLOB,
    refresh_token VARCHAR(256)
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



--
-- Table structure for table `oauth_code`
--
create table `oauth_code` (
    code VARCHAR(256), authentication BLOB
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

--------------------------------------------------
-- USER DATA TABLES:
--------------------------------------------------

--
-- Table structure for table `user`
--
CREATE TABLE `user` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `version` BIGINT(20) NOT NULL DEFAULT 0,
  `username` VARCHAR(50) NOT NULL,
  `password` VARCHAR(256) NOT NULL,
  `enabled` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

--
-- Table structure for table `role`
--
CREATE TABLE `role`(
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `version` BIGINT(20) NOT NULL DEFAULT 0,
  `name` VARCHAR(50) NOT NULL,
  `user_id` BIGINT(20) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `role_user_cs`
    FOREIGN KEY (`user_id`)
    REFERENCES `user` (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


--------------------------------------------------
-- MAPS DATA TABLES:
--------------------------------------------------
--
-- Table structure for table `level`
--
CREATE TABLE `level` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) DEFAULT NULL,
  `client_id` VARCHAR(256) NOT NULL,
  `name` VARCHAR(50) NOT NULL,
  `map_width` float(10,5) DEFAULT NULL,
  `map_height` float(10,5) DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE=InnoDB 
DEFAULT CHARSET=utf8;

--
-- Table structure for table `map_point`
--
CREATE TABLE `map_point` (
  `point_type` varchar(30) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) DEFAULT NULL,
  `level_id` BIGINT(20) NULL,
  `point_id` varchar(256) NOT NULL,
  `x` float(10,5) NOT NULL,
  `y` float(10,5) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `level_map_point_cs`
    FOREIGN KEY (`level_id`)
    REFERENCES `level` (`id`))
ENGINE=InnoDB 
DEFAULT CHARSET=utf8;

--
-- Table structure for table `marker`
--
CREATE TABLE `marker` (
  `marker_type` varchar(30) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `version` bigint(20) DEFAULT NULL,
  `point_id` BIGINT(20) NULL,
  `marker_id` varchar(256) NOT NULL,
  `marker_orientation_angle` double(8,6) DEFAULT NULL,
  `reference_image` varchar(256) DEFAULT NULL,
  `room_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `map_point_marker_cs`
    FOREIGN KEY (`point_id`)
    REFERENCES `map_point` (`id`))
ENGINE=InnoDB 
DEFAULT CHARSET=utf8;

--
-- Table structure for table `neighbours`
--
CREATE TABLE `neighbours` (
  `point_id` bigint(20) NOT NULL,
  `neighbour_id` bigint(20) NOT NULL,
  CONSTRAINT `neighbour_map_point_cs_1`
    FOREIGN KEY (`point_id`)
    REFERENCES `map_point` (`id`),
  CONSTRAINT `neighbour_map_point_cs_2`
    FOREIGN KEY (`neighbour_id`)
    REFERENCES `map_point` (`id`))
ENGINE=InnoDB
DEFAULT CHARSET=utf8;