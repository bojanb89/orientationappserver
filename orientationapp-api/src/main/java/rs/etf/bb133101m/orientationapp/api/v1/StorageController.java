/**
 * 
 */
package rs.etf.bb133101m.orientationapp.api.v1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import rs.etf.bb133101m.orientationapp.core.service.UserService;

/**
 * @author Bojan Bogojevic
 *
 */
@Controller
@RequestMapping("/storage")
public class StorageController {

	@Value("${storageHome}")
	private String storageHome;
	
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/**", method = RequestMethod.GET)
	public void getFile(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String restOfTheUrl = (String) request.getAttribute(HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);
		String relativePath = restOfTheUrl.replace("/storage", "/" + userService.currentOAuth2ClientId());

		Path documentPath = Paths.get(storageHome, relativePath);

		if (Files.exists(documentPath)) {
			// set header data
			response.setStatus(HttpStatus.OK.value());
			String probeContentType = Files.probeContentType(documentPath);
			response.setContentType(probeContentType);
			response.setContentLengthLong(Files.size(documentPath));

			try (ServletOutputStream outputStream = response.getOutputStream()) {
				Files.copy(documentPath, outputStream);
				outputStream.flush();
			}
		} else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}

	}
}
