/**
 * 
 */
package rs.etf.bb133101m.orientationapp.api.v1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.etf.bb133101m.orientationapp.core.service.UserService;

/**
 * @author Bojan Bogojevic
 *
 */
@RestController
public class HelloController {

	@Autowired
	private UserService userService;

	@RequestMapping("/test")
	public String home() {

		return "Hello, " + userService.currentOAuth2User().getUsername() + "Welcome";
	}
}
