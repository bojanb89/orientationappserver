package rs.etf.bb133101m.orientationapp.api;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hateoas.HypermediaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.Role;
import rs.etf.bb133101m.orientationapp.core.datamodel.entity.User;
import rs.etf.bb133101m.orientationapp.core.repository.UserRepository;
import rs.etf.bb133101m.orientationapp.core.security.UserDetailsImpl;

/**
 * Api Application
 *
 */
@SpringBootApplication(exclude = { HypermediaAutoConfiguration.class })
@PropertySource("${java:comp/env/orientationapp}")
@ComponentScan("rs.etf.bb133101m")
@EntityScan(basePackages = "rs.etf.bb133101m.orientationapp.core.datamodel")
@EnableJpaRepositories(basePackages = "rs.etf.bb133101m.orientationapp.core.repository")
@EnableTransactionManagement
@EnableResourceServer
public class OrientationAppApiApplication extends SpringBootServletInitializer {

	// this is needed for war packaging of the applicaiton
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(OrientationAppApiApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(OrientationAppApiApplication.class, args);
	}

	@Autowired
	public void init(AuthenticationManagerBuilder auth, UserRepository repository) throws Exception {
		if (repository.count() == 0) {
			User user = new User();
			user.setUsername("user");
			user.setPassword("password");
			user.setEnabled(true);
			Role role = new Role();
			role.setName("CLIENT");
			role.setUser(user);
			user.setRoles(Arrays.asList(role));
			repository.save(user);
		}
		auth.userDetailsService(userDetailsService(repository));
	}

	@Bean
	public UserDetailsService userDetailsService(final UserRepository repository) {
		return new UserDetailsService() {
			@Override
			public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
				return new UserDetailsImpl(repository.findByUsername(username));
			}
		};
	}

	@Configuration
	@EnableResourceServer
	protected static class ResourceServer extends ResourceServerConfigurerAdapter {

		@Autowired
		private TokenStore tokenStore;

		@Override
		public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
			resources.tokenStore(tokenStore);
		}

		@Override
		public void configure(HttpSecurity http) throws Exception {
			http.authorizeRequests().antMatchers("/storage/**").permitAll().and().authorizeRequests().anyRequest()
					.authenticated();
		}

	}
}
