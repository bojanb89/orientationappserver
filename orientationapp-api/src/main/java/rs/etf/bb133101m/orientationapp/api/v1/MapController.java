/**
 * 
 */
package rs.etf.bb133101m.orientationapp.api.v1;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import rs.etf.bb133101m.orientationapp.core.datamodel.to.MapTO;
import rs.etf.bb133101m.orientationapp.core.datamodel.to.PointTO;
import rs.etf.bb133101m.orientationapp.core.service.LevelService;
import rs.etf.bb133101m.orientationapp.core.service.MapPointService;
import rs.etf.bb133101m.orientationapp.core.service.StorageService;

/**
 * @author Bojan Bogojevic
 *
 */
@RestController
@RequestMapping(value = "/map")
public class MapController {

	@Autowired
	private MapPointService mapPointService;
	
	@Autowired
	private LevelService levelService;

	@Autowired
	private StorageService storageService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public void saveMap(@RequestParam("level") String levelName, @RequestBody MapTO mapTO) {
		mapPointService.savePoints(levelName, mapTO.pointTOs);
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public MapTO getMap(@RequestParam(value = "level", required = false) String levelName) {
		MapTO mapTO = new MapTO();
		List<PointTO> pointTOs = mapPointService.toFromPoints(levelName);
		mapTO.pointTOs = pointTOs;
	
		levelService.setMapDimensForMap(mapTO, levelName);
		
		return mapTO;
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public void uploadImage(@RequestParam("file") MultipartFile file, @RequestParam("level") String levelName) {

		String filename = file.getOriginalFilename();
		if (!file.isEmpty()) {
			try (InputStream inputStream = file.getInputStream()) {
				storageService.storeImage(levelName, filename, inputStream);
			} catch (IOException e) {
				e.printStackTrace();
				// TODO throw 500
			}
		} else {

			// TODO throw bad request exception
		}
	}
}
