/**
 * 
 */
package rs.etf.bb133101m.orientationapp.api;

/**
 * @author Bojan Bogojevic
 *
 */
public class Const {
	public static final String V1_URL_PREFIX = "/v1";
}
