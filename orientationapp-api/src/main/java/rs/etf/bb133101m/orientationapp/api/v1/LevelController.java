package rs.etf.bb133101m.orientationapp.api.v1;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.etf.bb133101m.orientationapp.core.datamodel.entity.map.Level;
import rs.etf.bb133101m.orientationapp.core.service.LevelService;

@RestController
@RequestMapping(value = "/level")
public class LevelController {
	
	@Autowired
	private LevelService levelService;

	@RequestMapping(value = "", method = RequestMethod.POST)
	public void addLevel(@RequestBody Level level) {
		levelService.addLevel(level);
	}
	
	@RequestMapping(value = "/dimens", method = RequestMethod.PUT)
	public void setLevelDimens(@RequestParam String levelName, @RequestParam Float mapWidth, @RequestParam Float mapHeight) {
		levelService.setLevelDimens(levelName, mapWidth, mapHeight);
	}

	@RequestMapping(value = "", method = RequestMethod.GET)
	public List<Level> getLevels() {
		return levelService.getLevels();
	}

	@RequestMapping(value = "", method = RequestMethod.DELETE)
	public void removeLevel(@RequestBody Level level) {
		levelService.removeLevel(level);
	}
}
